import 'package:shared_preferences/shared_preferences.dart';

class Mem {
  static Future<String> getS(key, defValue) async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(key)) {
      return prefs.getString(key) ?? defValue;
    } else {
      return Future<String>.value(defValue);
    }
  }

  static Future<int> getI(key, defValue) async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(key)) {
      return prefs.getInt(key) ?? defValue;
    } else {
      return defValue;
    }
  }


  static Future<bool> getB(key, bool defValue) async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(key)) {
      return prefs.getInt(key) == 1 ? true : false;
    } else {
      return defValue;
    }
  }

  static Future<double> getD(key, defValue) async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(key)) {
      return prefs.getDouble(key) ?? defValue;
    } else {
      return defValue;
    }
  }

  static Future<void> setS(key, value) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<void> setI(key, value) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

  static Future<void> setD(key, value) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setDouble(key, value);
  }

  static Future<void> setB(key, bool value) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value ? 1 : 0);
  }

  static void setIntInitialInt(key, value) {
    SharedPreferences.getInstance().then((prefs) => {
      if (!prefs.containsKey(key)) {prefs.setInt(key, value)}
    });
  }

  static void setIntInitialString(key, value) {
    SharedPreferences.getInstance().then((prefs) => {
      if (!prefs.containsKey(key)) {prefs.setString(key, value)}
    });
  }

  static Future<bool> isSet(key) async {
      var prefs = await SharedPreferences.getInstance();
      return prefs.containsKey(key);
  }

  static void delete(key) async{
    var prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}
