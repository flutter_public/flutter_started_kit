import 'package:flutter_starter/utils/fx.dart';

class GX {

  String appUrl = FX.APP_WEB_URL_INITIAL;
  double screenWidth = 400; // inital width

  static GX? _instance;
  factory GX() => _instance ??= GX._();
  GX._();
}
