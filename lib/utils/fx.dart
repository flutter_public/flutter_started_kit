// Global app variables

import 'package:flutter/material.dart';

class FX {
  static const String VERSION = '0.0.1';
  static const int VERSION_CODE = 1;
  static const String APP_WEB_URL_INITIAL="https://nintec.de";
  static const String APP_PPOLICY_URL = "https://nintec.de";

  static const String PREFS_THEME_INT_ID = "PREFS_THEME_INT_ID";

  static const List<Locale> SUPPORTED_LOCALES = [
    Locale('en'),
    Locale('de'),
  ];
  static const Locale FALLBACK_LOCALE = Locale('en');
  static const String ASSETS_I18M = 'assets/i18n';

  static const String THEME_LIGHT = 'light';
  static const String THEME_DARK = 'dark';
  static var COLOR_PRIMARY = Color(0xff440BD4);
  static var COLOR_INDICATOR = Color(0xffff2079);
  static var THEME_DATA_LIGHT = ThemeData.light().copyWith(
    primaryColor: COLOR_PRIMARY,
    primaryColorLight: COLOR_PRIMARY,
    primaryColorDark: COLOR_PRIMARY,
    indicatorColor: COLOR_INDICATOR,
    tabBarTheme: const TabBarTheme(
      labelColor: Colors.black,
    ),
    appBarTheme: const AppBarTheme(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        textTheme: TextTheme(
            headline6: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold)),
        titleTextStyle: TextStyle(
            color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold))
  );
}
