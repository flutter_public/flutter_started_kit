
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_starter/utils/fx.dart';
import 'package:flutter_starter/views/app/main_screen.dart';
import 'package:theme_provider/theme_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  runApp(
    EasyLocalization(
        supportedLocales: FX.SUPPORTED_LOCALES,
        fallbackLocale: FX.FALLBACK_LOCALE,
        path: FX.ASSETS_I18M,
        child: MyApp()),
  );
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ThemeProvider(
      saveThemesOnChange: true,
      loadThemeOnInit: false,
      onInitCallback: (controller, previouslySavedThemeFuture) async {
        String? savedTheme = await previouslySavedThemeFuture;
        if (savedTheme != null) {
          print("DEBUG_ load saved theme");
          controller.setTheme(savedTheme);
        } else {
          print("DEBUG_ load new theme");
          Brightness platformBrightness =
              SchedulerBinding.instance.window.platformBrightness;
          if (platformBrightness == Brightness.dark) {
            controller.setTheme(FX.THEME_DARK);
          } else {
            controller.setTheme(FX.THEME_LIGHT);
          }
          controller.forgetSavedTheme();
        }
      },
      themes: <AppTheme>[
        AppTheme.dark(id: 'dark'),
        AppTheme(
          id: 'light',
          description: 'custom light theme',
          data: FX.THEME_DATA_LIGHT,
        ),
      ],
      child: ThemeConsumer(
        child: Builder(
          builder: (themeContext) => MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeProvider.themeOf(themeContext).data,
            home: MainScreen(themeId: ThemeProvider.themeOf(themeContext).id),
            locale: context.locale,
            supportedLocales: context.supportedLocales,
            localizationsDelegates: context.localizationDelegates,
          ),
        ),
      ),
    );
  }
}