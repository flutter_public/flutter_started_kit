import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_starter/utils/gx.dart';
import 'package:flutter_starter/views/helpers/bottom_nav.dart';
import 'package:flutter_starter/views/home/home_nav.dart';
import 'package:flutter_starter/views/news/news_nav.dart';
import 'package:flutter_starter/views/settings/settings_nav.dart';

class MainScreen extends StatefulWidget {
  final String? themeId;

  const MainScreen({Key? key, this.themeId}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int selectedIndex = 0;

  final _navis = [const HomeNav(), const NewsNav(), const SettingsNav()];

  @override
  void initState() {
    super.initState();

    // TODO:
    // Get Server Data
    // Init FCM here
  }

  @override
  Widget build(BuildContext context) {
    GX().screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: _navis[selectedIndex],
      bottomNavigationBar: NavigationBarTheme(
        data: const NavigationBarThemeData(indicatorColor: Colors.red),
        child: BottomNav(
          objects: [
            NavObject(Icons.home_outlined, Icons.home, tr('abc_home')),
            NavObject(Icons.account_balance_outlined, Icons.account_balance, tr('abc_news')),
            NavObject(Icons.settings_outlined, Icons.settings, tr('abc_settings')),
          ],
          initialIndex: 0,
          iconColor: Colors.grey,
          activeColor: Colors.blue,
          selectedCallback: (index) {
            setState(() {
              selectedIndex = index;
            });
          },
        ),
      ),
    );
  }

// _changeTheme(){
//   var controller = ThemeProvider.controllerOf(context);
//   if(ThemeProvider.themeOf(context).id == FX.THEME_LIGHT){
//     controller.setTheme(FX.THEME_DARK);
//     _themeIcon = Icons.dark_mode;
//   } else {
//     controller.setTheme(FX.THEME_LIGHT);
//     _themeIcon = Icons.light_mode;
//   }
// }
}
