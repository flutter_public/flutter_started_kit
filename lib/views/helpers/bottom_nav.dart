import 'package:flutter/material.dart';

class BottomNav extends StatefulWidget {
  final int initialIndex;
  final List<NavObject> objects;
  final Color iconColor;
  final Color activeColor;
  final Function selectedCallback;

  const BottomNav(
      {Key? key,
      required this.initialIndex,
      required this.objects,
      required this.iconColor,
      required this.activeColor,
      required this.selectedCallback})
      : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int? selected;
  List<Widget>? buttons;

  @override
  void initState() {
    super.initState();
    selected = widget.initialIndex;
  }

  @override
  Widget build(BuildContext context) {
    buttons = [];
    int x = 0;
    for (NavObject no in widget.objects) {
      buttons!.add(_button(x, no));
      x++;
    }
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Material(
        child: Ink(
          color: Theme.of(context).cardColor,
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: buttons!,
          ),
        ),
      ),
    );
  }

  Widget _button(int index, NavObject object) {
    return Expanded(
      child: InkWell(
        customBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        onTap: () {
          setState(() {
            selected = index;
            widget.selectedCallback(index);
          });
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                index == selected ? object.activeIconData : object.iconData,
                color:
                    index == selected ? widget.activeColor : widget.iconColor,
              ),
              Text(
                object.text,
                style: TextStyle(
                    color: index == selected
                        ? widget.activeColor
                        : widget.iconColor),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class NavObject {
  IconData iconData;
  IconData activeIconData;
  String text;

  NavObject(this.iconData, this.activeIconData, this.text);
}
