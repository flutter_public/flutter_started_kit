import 'package:flutter/material.dart';

class MaterialIndicator extends Decoration {
  MaterialIndicator();

  @override
  _CustomPainter createBoxPainter([VoidCallback? onChanged]) {
    return new _CustomPainter(onChanged);
  }
}

class _CustomPainter extends BoxPainter {

  double px = 10.0;
  double height = 4.0;

  _CustomPainter(VoidCallback? onChanged) : super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    //offset is the position from where the decoration should be drawn.
    //configuration.size tells us about the height and width of the tab.
    Size mysize =
    Size(configuration.size!.width - px * 2, height);

    Offset myoffset = Offset(
        offset.dx + px,
        offset.dy + configuration.size!.height - height);

    final Rect rect = myoffset & mysize;
    final Paint paint = Paint();
    paint.color = Colors.green;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 1;
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          rect,
          bottomRight: Radius.circular(0),
          bottomLeft: Radius.circular(0),
          topLeft: Radius.circular(4),
          topRight: Radius.circular(4),
        ),
        paint);
  }
}

enum TabPosition { top, bottom }