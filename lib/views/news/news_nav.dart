import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_starter/views/helpers/tab_indicator.dart';

class NewsNav extends StatefulWidget {
  const NewsNav({Key? key}) : super(key: key);

  static TabBar tabBar = TabBar(
    isScrollable: true,
    tabs: [
      Tab(text: tr('abc_tab_1')),
      Tab(text: tr('abc_tab_2')),

    ],
    indicator: MaterialIndicator(),
  );

  @override
  State<NewsNav> createState() => _NewsNavState();
}

class _NewsNavState extends State<NewsNav> {




  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: NewsNav.tabBar.tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(tr('abc_news')),
          bottom: NewsNav.tabBar,
        ),
        body: TabBarView(
          children: [
            Container(color: Colors.red),
            Container(color: Colors.blue),
          ],
        ),
        // bottomNavigationBar: AdsBanner(context: context,),
      ),
    );
  }
}