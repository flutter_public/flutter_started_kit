import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_starter/utils/fx.dart';
import 'package:flutter_starter/views/settings/dark_light_mode.dart';
import 'package:flutter_starter/views/settings/version_link.dart';

class SettingsNav extends StatelessWidget {
  const SettingsNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr('abc_settings')),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            DarkLightMode(),
            VersionLink(),
          ],
        ),
      ),
    );
  }
}