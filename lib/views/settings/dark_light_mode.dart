import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_starter/utils/fx.dart';
import 'package:flutter_starter/utils/mem.dart';
import 'package:theme_provider/theme_provider.dart';

class DarkLightMode extends StatefulWidget {
  const DarkLightMode({super.key});

  @override
  _DarkLightModeState createState() => _DarkLightModeState();
}

class _DarkLightModeState extends State<DarkLightMode> {
  int selected = 0;
  late ThemeController controller;

  @override
  void initState() {
    super.initState();
    _initThemePars();
  }

  void _initThemePars() async {
    var sel = await Mem.getI(FX.PREFS_THEME_INT_ID, 0);
    setState(() {
      selected = sel;
    });
  }

  @override
  Widget build(BuildContext context) {
    controller = ThemeProvider.controllerOf(context);

    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
              leading: const Icon(Icons.wb_sunny_outlined),
              title: Text(tr('abc_app_theme'),
                  style: const TextStyle(fontWeight: FontWeight.bold))),
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () => {_setSystemTheme()},
                  child: Card(
                    shape: selected == 0
                        ? RoundedRectangleBorder(
                            side:
                                 const BorderSide(color: Colors.pink, width: 4.0),
                            borderRadius: BorderRadius.circular(4.0))
                        :  RoundedRectangleBorder(
                            side:
                                 const BorderSide(color: Colors.grey, width: 2.0),
                            borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        height: 60,
                        child: Center(
                            child: Column(
                          children: [
                            const Icon(Icons.brightness_4),
                            Text(tr('abc_look_phone')),
                          ],
                        )),
                      ),
                    ),
                  ),
                ),
              ),
              // Light Mode
              Expanded(
                child: InkWell(
                  onTap: () => {_setLightTheme()},
                  child: Card(
                    shape: selected == 1
                        ? RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Colors.pink, width: 4.0),
                            borderRadius: BorderRadius.circular(4.0))
                        : RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Colors.grey, width: 2.0),
                            borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        height: 60,
                        child: Center(
                            child: Column(
                          children: [
                            const Icon(Icons.brightness_5),
                            Text(tr('abc_look_light')),
                          ],
                        )),
                      ),
                    ),
                  ),
                ),
              ),
              // Dark Mode
              Expanded(
                child: InkWell(
                  onTap: () => {_setDarkTheme()},
                  child: Card(
                    color: Colors.black54,
                    shape: selected == 2
                        ? RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Colors.pink, width: 4.0),
                            borderRadius: BorderRadius.circular(4.0))
                        : RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Colors.grey, width: 2.0),
                            borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        height: 60,
                        child: Center(
                            child: Column(
                          children: [
                            const Icon(Icons.bedtime, color: Colors.white),
                            Text(
                              tr('abc_look_dark'),
                              style: const TextStyle(color: Colors.white),
                            ),
                          ],
                        )),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          )
        ],
      ),
    );
  }

  _setSystemTheme() {
    Brightness platformBrightness =
        SchedulerBinding.instance.window.platformBrightness;
    if (platformBrightness == Brightness.dark) {
      controller.setTheme('dark');
    } else {
      controller.setTheme('light');
    }
    controller.forgetSavedTheme();

    setState(() {
      Mem.setI(FX.PREFS_THEME_INT_ID, 0);
      selected = 0;
    });
  }

  _setLightTheme() {
    controller.setTheme('light');
    setState(() {
      Mem.setI(FX.PREFS_THEME_INT_ID, 1);
      selected = 1;
    });
  }

  _setDarkTheme() {
    controller.setTheme('dark');
    setState(() {
      Mem.setI(FX.PREFS_THEME_INT_ID, 2);
      selected = 2;
    });
  }
}
