import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_starter/utils/fx.dart';
import 'package:flutter_starter/utils/gx.dart';
import 'package:url_launcher/url_launcher.dart';

class VersionLink extends StatelessWidget {
  const VersionLink({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text(
                tr('abc_app'),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              leading: Icon(Platform.isAndroid ? Icons.android : Icons.apple),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: OutlinedButton(
                  onPressed: () {
                    launch(GX().appUrl);
                  },
                  child: Text(Platform.isAndroid ? tr('abc_play_store') : tr('abc_app_store') )),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: OutlinedButton(
                onPressed: () {
                  launch(FX.APP_PPOLICY_URL);
                },
                child: Text(tr('abc_privacy_policy')),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 16, bottom: 16),
              child: Text('Version: ${FX.VERSION}, Code: ${FX.VERSION_CODE}'),
            ),

          ],
        )
    );
  }
}