import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class HomeNav extends StatelessWidget {
  const HomeNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(tr('text_home')),
    );
  }
}
